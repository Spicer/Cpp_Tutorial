


int main(void)
{
	// Appending and concatenating strings:
	std::string a_str("45");
    std::string b_str("11");
 
    std::cout << a_str + b_str << "\n"; // a and b will be appended, not added
    a_str += " volts";
    std::cout << a << "\n";


    // C style strings
	char mystring[] = "string";
    std::cout << mystring << " has " << sizeof(mystring) << " characters.\n";
    for (int index = 0; index < sizeof(mystring); ++index)
        std::cout << static_cast<int>(mystring[index]) << " ";


    char myname[20] = "Alex"; // only use 5 characters (4 letters + null terminator)
    std::cout << "My name is: " << myname << '\n';
    std::cout << myname << " has " << strlen(myname) << " letters.\n";
    std::cout << myname << " has " << sizeof(myname) << " characters in the array.\n";


    mystring[1] = 'p';
    std::cout << std::endl << mystring << std::endl;


    char source[] = "Copy this!";
    char dest[20]; // note that the length of dest is only 4 chars!
    strcpy(dest, source); // An assert will be thrown in debug mode
    std::cout << dest << std::endl;


    // Ask the user to enter a string
    char buffer[255];
    std::cout << "Enter a string: ";
    std::cin.getline(buffer, 255);

 
    int spacesFound = 0;
    // Loop through all of the characters the user entered
    for (int index = 0; index < strlen(buffer); ++index)
    {
        // If the current character is a space, count it
        if (buffer[index] == ' ')
            spacesFound++;
    }
 
    std::cout << "You typed " << spacesFound << " spaces!\n";

    std::cout << buffer << " has " << buffer.length() << " characters\n";


    char name_cstr[255]; // declare array large enough to hold 255 characters
    std::cout << "Enter your name: ";
    std::cin.getline(name_cstr, 255);
    std::cout << "You entered: " << name_cstr << '\n';


    return 0;
}