// #include <utilities>
#include <algorithm>
#include <iostream>
#include <array>
#include <vector>

// When an array is passed to a function, it decays into a pointer to save on having to copy the whole array.
// Therefore, we cannot find the size of the array inside the function. Additionally, any changes made to the array are kept.
// When declaring a finction, both "void printSize(int array[]);" and "void printSize(int *array);" are identical.

// Finally, it is worth noting that arrays that are part of structs or classes do not decay when the whole struct or class is passed to a function.
// This yields a useful way to prevent decay if desired, and will be valuable later when we write classes that utilize arrays.

// If ptr points to an integer, ptr + 1 is the address of the next integer in memory after ptr. ptr - 1 is the address of the previous integer before ptr.
// When calculating the result of a pointer arithmetic expression, the compiler always multiplies the integer operand by the size of the object being pointed to.
// This is called scaling.
// Adding 1 to an array should point to the second element (element 1) of the array.
// It turns out that when the compiler sees the subscript operator ([]), it actually translates that into a pointer addition and dereference!
// Generalizing, array[n] is the same as *(array + n), where n is an integer. The subscript operator [] is there both to look nice and for ease of use.


// C++11 introduces a new type of loop called a for-each loop (also called a range-based for loop) that provides a simpler and safer method
// for cases where we want to iterate through every element in an array (or other list-type structure).
// The for-each statement has a syntax that looks like this:
// for (element_declaration : array)
//    statement;

// Because element_declaration should have the same type as the array elements,
// this is an ideal case in which to use the auto keyword, and let C++ deduce the type of the array elements for us.
void fibonacci(void)
{
	int fibonacci[] = { 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89 };
    for (auto number : fibonacci) // iterate over array fibonacci
       std::cout << number << ' '; // we access the array element for this iteration through variable number
   std::cout << "\n";
}

// In the for-each example above, our element declarations are declared by value. This means each array element iterated over will be copied into variable element.
// Copying array elements can be expensive, and most of the time we really just want to refer to the original element. Fortunately, we can use references for this:
void printWithForEach(void)
{
	int array[5] = { 9, 7, 5, 3, 1 };
    for (auto &element: array) // The ampersand makes element a reference to the actual array element, preventing a copy from being made
        std::cout << element << ' ';
    std::cout << "\n";
}
// In the above example, element will be a reference to the currently iterated array element, avoiding having to make a copy.
// Also any changes to element will affect the array being iterated over, something not possible if element is a normal variable.
// And, of course, it’s a good idea to make your element const if you’re intending to use it in a read-only fashion.

// In order to iterate through the array, for-each needs to know how big the array is, which means knowing the array size.
// Because arrays that have decayed into a pointer do not know their size, for-each loops will not work with them!


void findName(void)
{
	const std::string names[] = { "Alex", "Betty", "Caroline", "Dave", "Emily", "Fred", "Greg", "Holly" };
	
    std::cout << "Enter a name: ";
    std::string username;
    std::cin >> username;
 
    bool found(false);
    for (const auto &name : names)
        if (name == username)
        {
            found = true;
            break;
        }
 
    if (found)
        std::cout << username << " was found.\n";
    else
        std::cout << username << " was not found.\n";
}


enum Type
{
    INT,
    FLOAT,
    CSTRING
};
 
void printValue(void *ptr, Type type)
{
    switch (type)
    {
        case INT:
            std::cout << *static_cast<int*>(ptr) << '\n'; // cast to int pointer and dereference
            break;
        case FLOAT:
            std::cout << *static_cast<float*>(ptr) << '\n'; // cast to float pointer and dereference
            break;
        case CSTRING:
            std::cout << static_cast<char*>(ptr) << '\n'; // cast to char pointer and dereference
            break;
    }
}


// Sort in ascending order using the bubble sort algorithm
void bubbleSort(int array[], int length)
{
	bool swap = false;
	for(int ii=0; ii<length-1; ++ii)
	{
		for(int jj=0; jj<length-1-ii; ++jj)
		{
			if(array[jj] > array[jj+1])
			{
				std::swap(array[jj], array[jj+1]);
				swap = true;
			}
		}
		if(!swap)
		{
			std::cout << "Early termination on iteration " << ii+1 << "\n";
			break;
		}
		swap = false;
	}
}


void printArray(int array[], int length)
{
	for(int ii=0; ii<length; ++ii)
	{
		std::cout << array[ii] << " ";
	}
	std::cout << "\n";
}


void printMat(void)
{
	// Declare a 10x10 array
    const int numRows = 10;
    const int numCols = 10;
    int product[numRows][numCols] = { 0 };
 
    // Calculate a multiplication table
    for (int row = 0; row < numRows; ++row)
        for (int col = 0; col < numCols; ++col)
            product[row][col] = row * col;
 
    // Print the table
    for (int row = 1; row < numRows; ++row)
    {
        for (int col = 1; col < numCols; ++col)
            std::cout << product[row][col] << "\t";
 
        std::cout << '\n';
    }
}


// str will point to the first letter of the C-style string.
// Note that str points to a const char, so we can not change the values it points to.
// However, we can point str at something else.  This does _not_ change the value of the argument.
void printCString(const char *str)
{
	// While we haven't encountered a null terminator
	while (*str != '\0')
	{
		// print the current character
		std::cout << *str;
 
		// and point str at the next character
		++str;
	}
}


int main()
{
	const int length(9);
	int array[length] = { 6, 3, 2, 9, 7, 1, 5, 4, 8 };

	bubbleSort(array, length);
	printArray(array, length);

 
    // print the value of the array variable
    std::cout << "The array has address: " << array << '\n';
 
    // print address of the array elements
    std::cout << "Element 0 has address: " << &array[0] << '\n';


    // dereferencing an array returns the first element (element 0)
	std::cout << *array << "\n"; // will print 1!

	int *ptr = array;
    std::cout << *ptr << "\n"; // will print 1


	std::cout << sizeof(array) << '\n'; // will print sizeof(int) * array length

    std::cout << sizeof(ptr) << '\n'; // will print the size of a pointer\



    std::cout << &array[1] << '\n'; // print memory address of array element 1
	std::cout << array+1 << '\n'; // print memory address of array pointer + 1 

	std::cout << array[1] << '\n'; // prints 2
	std::cout << *(array+1) << '\n'; // prints 2 (note the parenthesis required here)

	fibonacci();

	printWithForEach();


	int nValue = 5;
    float fValue = 7.5;
    char szValue[] = "Mollie";
 
    printValue(&nValue, INT);
    printValue(&fValue, FLOAT);
    printValue(szValue, CSTRING);


    printCString("Hello, World!");


    //std::array
    std::array<int, 5> myarray = { 9, 7, 5, 3, 1 }; // initialization list
	std::array<int, 5> myarray2 { 9, 7, 5, 3, 1 }; // uniform initialization

	myarray[2] = 6;
	myarray.at(1) = 6; // if array element 1 valid, sets array element 1 to value 6 - safer (and slower) than []
	// Because std::array doesn’t decay to a pointer when passed to a function, the size() function will work even if you call it from within a function
	// Rule: Always pass std::array by reference or const reference

	for (auto &element : myarray)
	    std::cout << element << ' ';
	std::cout <<"\n";

	std::sort(myarray.begin(), myarray.end()); // sort the array forwards - these arguments are iterators
//    std::sort(myarray.rbegin(), myarray.rend()); // sort the array backwards


	// std::vector
	// std::vector provides dynamic array functionality that handles its own memory management.
	// This means you can create arrays that have their length set at runtime, without having to explicitly allocate and deallocate memory using new and delete. 
	std::vector<int> vec1 = { 9, 7, 5, 3, 1 }; // use initializer list to initialize array
	std::vector<int> vec2 { 9, 7, 5, 3, 1 }; // use uniform initialization to initialize array (C++11 onward)
	vec1[3] = 2; // no bounds checking
	// vec1.at(7) = 3; // does bounds checking

	vec1.resize(7); // set size to 5
    std::cout << "The length is: " << vec1.size() << '\n';
    for (auto const &element: vec1)
        std::cout << element << ' ';
    std::cout << "\n";

    // Resizing a vector is computationally expensive, so you should strive to minimize the number of times you do so.

    // There is a special implementation for std::vector of type bool that will compact 8 booleans into a byte!
    // This happens behind the scenes, and is largely transparent to you as a programmer.
    std::vector<bool> bool_vec { true, false, false, true, true };
    std::cout << "The length is: " << bool_vec.size() << '\n';
 
    for (auto const &element: bool_vec)
        std::cout << element << ' ';
    std::cout << "\n";

	return 0;
}