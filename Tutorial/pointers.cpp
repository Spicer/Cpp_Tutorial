// Best practice: With C++11, use nullptr to initialize your pointers to a null value.
// int *ptr = nullptr; // note: ptr is still an integer pointer, just set to a null value (0)

// References must always be initialized with a valid object. Unlike pointers, which can hold a null value, there is no such thing as a null reference.
// Because references are implicitly const, a reference can not be “redirected” (assigned) to reference another variable.


// When argument n is passed to the function, the function parameter ref is set as a reference to argument n.
// This allows the function to change the value of n through ref! Note that n does not need to be a reference itself.
// This is useful both when the function needs to change the value of an argument passed in, or when making a copy of the argument would be expensive (e.g. for a large struct).
// References as function parameters can also be const. This allows us to access the argument without making a copy of it,
// while guaranteeing that the function will not change the value being referenced.
// To avoid making unnecessary, potentially expensive copies, variables that are not pointers and not fundamental data types (int, double, etc…)
// should be generally passed by (const) reference.


// A secondary use of references is to provide easier access to nested data. Let’s say we needed to work with the "value1" field of the "Something" struct of "Other".
// Normally, we’d access that member as "other.something.value1". If there are many separate accesses to this member, the code can become messy.
// References allow you to more easily access the member:
// int &ref = other.something.value1;
// // ref can now be used in place of other.something.value1
// The following two statements are thus identical:
// other.something.value1 = 5;
// ref = 5;
// This can help keep your code cleaner and more readable.


// Because the syntax for access to structs and class members through a pointer is awkward, C++ offers a second member selection operator (->) for doing member selection from pointers.
// The following two lines are equivalent:
// (*ptr).age  = 5;
// ptr->age = 5;


#include <iostream>


// ref is a reference to the argument passed in, not a copy
void changeN(int &ref)
{
	ref = 6;
}

// ref is a const reference to the argument passed in, not a copy
void takeConstRef(const int &ref)
{
	// ref = 6; // not allowed, ref is const
}


int main(void)
{
	const int value = 5;
	const int *ptr = &value; // this is okay, ptr is pointing to a "const int"
	// *ptr = 6; // not allowed, we can't change a const value


	// Because a pointer to a const value is not const itself (it just points to a const value), the pointer can be redirected to point at other values:
	int value2 = 6;
	ptr = &value2; // okay, ptr now points at some other const int


	// To declare a const pointer, use the const keyword between the asterisk and the pointer name:
	int *const const_ptr = &value2;
	const int *const const_ptr2 = &value;


	int &ref = value2; // reference to variable value
	value2 = 7; // value is now 7
	ref = 8; // value is now 8
	
	std::cout << value2 << "\n"; // prints 8
	++ref;
	std::cout << value2 << "\n"; // prints 9

	std::cout << &value2 << "\n"; // prints 0x7ffc197e00d4
	std::cout << &ref << "\n"; // prints 0x7ffc197e00d4


	const int &const_ref = value; // ref is a reference to const value


	int n = 5;
	std::cout << n << '\n';
	changeN(n); // note that this is a non-reference argument
	std::cout << n << '\n';


	// pointers to pointers:
	int (*array)[5] = new int[10][5];
	int **array2 = new int*[10]; // allocate an array of 10 int pointers — these are our rows
	for (int count = 0; count < 10; ++count)
	    array2[count] = new int[count+1]; // these are our columns

	for (int count = 0; count < 10; ++count)
	    delete[] array2[count];
	delete[] array2; // this needs to be done last
	// Note that we delete the array in the opposite order that we created it.If we delete array before the array elements,
	// then we’d have to access deallocated memory to delete the array elements. And that would result in undefined behavior.
	

	return 0;
}