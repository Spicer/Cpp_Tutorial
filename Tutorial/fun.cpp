//http://www.learncpp.com/cpp-tutorial

#include <iostream>
#include <iomanip> // for std::setprecision()
#include <cmath> // needed for pow()
#include <cstring>
#include "constants.h"
#include <cstdint> //includes things such as int16_t, etc.
#include <bitset>
#include <typeinfo> // for typeid()
#include <string>
#include <cstdlib> // needed for exit(), srand() and rand()
#include <random> // for std::random_device and std::mt19937
#include <ctime> // for time()

int value(5); // global variable
int g_value(5); //global variable where g_ prevents naming conflicts with local variables
static int g_x; // g_x is static, and can only be used within this file
extern double g_y(9.8); // g_y is external, and can be used by other files
//By default, non-const variables declared outside of a block are assumed to be external. However, const variables declared outside of a block are assumed to be internal.
//In order to use an external global variable that has been declared in another file, you have to use a variable forward declaration.
//For variables, creating a forward declaration is also done via the extern keyword (with no initialization value).





static double g_gravity (9.8); // restrict direct access to this file only
double getGravity() // this function can be exported to other files to access the global outside of this file
{
    // We could add logic here if needed later
    return g_gravity;
} 



// This function is declared as static, and can now be used only within this file
// Attempts to access it via a function prototype will fail
static int add(int x, int y)
{
    return x + y;
}


// note: exp must be non-negative
int intPow(int base, int exp)
{
    int result = 1;
    while (exp)
    {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        base *= base;
    }
 
    return result;
}


void incrementDecrement(void)
{
	int x = 5, y = 5;
	std::cout << x << " " << y << std::endl;
	std::cout << ++x << " " << --y << std::endl; // prefix
	std::cout << x << " " << y << std::endl;
	std::cout << x++ << " " << y-- << std::endl; // postfix
	std::cout << x << " " << y << std::endl;
}


void listTypeSizes(void)
{
	std::cout << "bool:\t\t" << sizeof(bool) << " bytes" << std::endl;
    std::cout << "char:\t\t" << sizeof(char) << " bytes" << std::endl;
    std::cout << "wchar_t:\t" << sizeof(wchar_t) << " bytes" << std::endl;
    std::cout << "char16_t:\t" << sizeof(char16_t) << " bytes" << std::endl; // C++11, may not be supported by your compiler
    std::cout << "char32_t:\t" << sizeof(char32_t) << " bytes" << std::endl; // C++11, may not be supported by your compiler
    std::cout << "short:\t\t" << sizeof(short) << " bytes" << std::endl;
    std::cout << "int:\t\t" << sizeof(int) << " bytes" << std::endl;
    std::cout << "long:\t\t" << sizeof(long) << " bytes" << std::endl;
    std::cout << "long long:\t" << sizeof(long long) << " bytes" << std::endl; // C++11, may not be supported by your compiler
    std::cout << "float:\t\t" << sizeof(float) << " bytes" << std::endl;
    std::cout << "double:\t\t" << sizeof(double) << " bytes" << std::endl;
    std::cout << "long double:\t" << sizeof(long double) << " bytes" << std::endl;
}


void printBools(void)
{
	std::cout << true << std::endl;
    std::cout << false << std::endl;
 
    std::cout << std::boolalpha; // print bools as true or false
 
    std::cout << true << std::endl;
    std::cout << false << std::endl;

}


void conditionals(void)
{
	// (condition) ? expression : other_expression;

	// if (condition)
	//     expression;
	// else
	//     other_expression;


	int x = 3;
	int y = 4;
	int larger;
	// x = (condition) ? some_value : some_other_value;
	if (x > y)
	    larger = x;
	else
	    larger = y;
		
	larger = (x > y) ? x : y;


	bool inBigClassroom = false;
	const int classSize = inBigClassroom ? 30 : 20;
}


// return true if the difference between a and b is within epsilon percent of the larger of a and b
bool approximatelyEqual(double a, double b, double epsilon)
{
    return fabs(a - b) <= ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * epsilon);
}
// return true if the difference between a and b is less than absEpsilon, or within relEpsilon percent of the larger of a and b
bool approximatelyEqualAbsRel(double a, double b, double absEpsilon, double relEpsilon)
{
    // Check if the numbers are really close -- needed when comparing numbers near zero.
    double diff = fabs(a - b);
    if (diff <= absEpsilon)
        return true;
 
    // Otherwise fall back to Knuth's algorithm
    return diff <= ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * relEpsilon);
}


void useBitset(void)
{
	const int option_1 = 0;
	const int option_2 = 1;
	const int option_3 = 2;
	const int option_4 = 3;
	const int option_5 = 4;
	const int option_6 = 5;
	const int option_7 = 6;
	const int option_8 = 7;

	std::bitset<8> bits(0x2); // we need 8 bits, start with bit pattern 0000 0010
    bits.set(option_5); // set bit 4 to 1 (now we have 0001 0010)
    bits.flip(option_6); // flip bit 5 (now we have 0011 0010)
    bits.reset(option_6); // set bit 5 back to 0 (now we have 0001 0010)
 
    std::cout << "Bit 4 has value: " << bits.test(option_5) << '\n';
    std::cout << "Bit 5 has value: " << bits.test(option_6) << '\n';
    std::cout << "All the bits: " << bits << '\n';
}

void bitMask(void)
{
	const unsigned int lowMask = 0xF; // bit mask to keep low 4 bits (hex for 0000 0000 0000 1111)
 
    std::cout << "Enter an integer: ";
    int num;
    std::cin >> num;
 
    num &= lowMask; // remove the high bits to leave only the low bits
 
    std::cout << "The 4 low bits have value: " << num << '\n';


    //RGBA (red, green, blue, alpha) information is often stored in a single 32-bit integer
    const unsigned int redBits = 0xFF000000;
    const unsigned int greenBits = 0x00FF0000;
    const unsigned int blueBits = 0x0000FF00;
    const unsigned int alphaBits = 0x000000FF;
 
    std::cout << "Enter a 32-bit RGBA color value in hexadecimal (e.g. FF7F3300): ";
    unsigned int pixel;
    std::cin >> std::hex >> pixel; // std::hex allows us to read in a hex value
 
    // use bitwise AND to isolate red pixels, then right shift the value into the range 0-255
    unsigned char red = (pixel & redBits) >> 24;
    unsigned char green = (pixel & greenBits) >> 16;
    unsigned char blue = (pixel & blueBits) >> 8;
    unsigned char alpha = pixel & alphaBits;
 
    std::cout << "Your color contains:\n";
    std::cout << static_cast<int>(red) << " of 255 red\n";
    std::cout << static_cast<int>(green) << " of 255 green\n";
    std::cout << static_cast<int>(blue) << " of 255 blue\n";
    std::cout << static_cast<int>(alpha) << " of 255 alpha\n";
}


// Use persistent local variables (static)
void incrementAndPrint()
{
    static int s_value = 1; // static duration via static keyword.  This line is only executed once.
    ++s_value; //s_ indicates a static variable
    std::cout << s_value << std::endl;
} // s_value is not destroyed here, but becomes inaccessible

//used to give unique IDs
int generateID()
{
    static int s_itemID = 0;
    return s_itemID++; // makes copy of s_itemID, increments the real s_itemID, then returns the value in the copy
}


// Generate a random number between min and max (inclusive)
// Assumes srand() has already been called
int getRandomNumber(int min, int max)
{
    static const double fraction = 1.0 / (static_cast<double>(RAND_MAX) + 1.0);  // static used for efficiency, so we only calculate this value once
    // evenly distribute the random number across our range
    return static_cast<int>(rand() * fraction * (max - min + 1) + min);
}


int main()
{

	int value = 7; // hides the global variable value
    value++; // increments local value, not global value
    ::value--; // decrements global value, not local value
 
    std::cout << "global value: " << ::value << "\n";
    std::cout << "local value: " << value << "\n";


    std::cout << "The value of pi is " << Constants::pi << std::endl;


    listTypeSizes();


    printBools();


    char ch(97);
    std::cout << ch << std::endl;
    std::cout << static_cast<int>(ch) << std::endl;
    std::cout << ch << std::endl;


    unsigned short x = 65535; // largest 16-bit unsigned value possible
    std::cout << "x was: " << x << std::endl;
    x = x + 1; // 65536 is out of our range -- we get overflow because x can't hold 17 bits
    std::cout << "x is now: " << x << std::endl;


    float a = 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1;
    std::cout << approximatelyEqual(a, 1.0, 1e-8) << "\n";     // compare "almost 1.0" to 1.0
    std::cout << approximatelyEqual(a-1.0, 0.0, 1e-8) << "\n"; // compare "almost 0.0" to 0.0
    std::cout << approximatelyEqualAbsRel(a-1.0, 0.0, 1e-12, 1e-8) << "\n"; // compare "almost 0.0" to 0.0


    useBitset();


    bitMask();


    incrementAndPrint();
    incrementAndPrint();
    incrementAndPrint();

    generateID();


    short a_s(4);
    short b_s(5);
    std::cout << typeid(a_s + b_s).name() << " " << a_s + b_s << std::endl; // show us the type of a + b

    double d(4.0);
    short s(2);
    std::cout << typeid(d + s).name() << " " << d + s << std::endl; // show us the type of d + s
    // In this case, the short undergoes integral promotion to an int. However, the int and double still do not match.
    // Since double is higher on the hierarchy of types, the integer 2 gets converted to the double 2.0, and the doubles are added to produce a double result.
    // The priority of operands is as follows:
	    // long double (highest)
	    // double
	    // float
	    // unsigned long long
	    // long long
	    // unsigned long
	    // long
	    // unsigned int
	    // int (lowest)

    // This hierarchy can cause some interesting issues. For example, take a look at the following code:
	std::cout << 5u - 10 << " is not the same as -5\n"; // 5u means treat 5 as an unsigned integer



	// Multiple declarations
	int iii, jjj;
    for (iii=0, jjj=9; iii < 10; ++iii, --jjj)
        std::cout << iii << " " << jjj << std::endl;

    for (int count=0; count  < 20; ++count)
	{
	    // if the number is divisible by 4, skip this iteration
	    if ((count % 4) == 0)
	        continue; // jump to end of loop body
	 
	    // If the number is not divisible by 4, keep going
	    std::cout << count << std::endl;
	 
	    // The continue statement jumps to here
	}

	int count(0);
	do
	{
	    if (count == 5)
	        continue; // jump to end of loop body
	    std::cout << count << " ";
	 
	    // The continue statement jumps to here
	} while (++count < 10); // this still executes since it's outside the loop body


	srand(static_cast<unsigned int>(time(0))); // set initial seed value to system clock
 
    for (int count=0; count < 100; ++count)
    {
        std::cout << rand() << "\t";
 
        // If we've printed 5 numbers, start a new row
        if ((count+1) % 5 == 0)
            std::cout << "\n";
	}


	std::random_device rd; // Use a hardware entropy source if available, otherwise use PRNG
    std::mt19937 mersenne(rd()); // initialize our mersenne twister with a random seed
 
    // Print a bunch of random numbers
    for (int count = 0; count < 48; ++count)
    {
        std::cout << mersenne() << "\t";
 
        // If we've printed 4 numbers, start a new row
        if ((count + 1) % 4 == 0)
            std::cout << "\n";
    }


    


    exit(0); // terminate and return 0 to operating system

    return 0;
}