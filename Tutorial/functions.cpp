// Advantages of passing by value:
//     Arguments passed by value can be variables (e.g. x), literals (e.g. 6), expressions (e.g. x+1), structs & classes, and enumerators.
//     Arguments are never changed by the function being called, which prevents side effects. 

// Disadvantages of passing by value:
//     Copying structs and classes can incur a significant performance penalty, especially if the function is called many times. 

// When to use pass by value:
//     When passing fundamental data type and enumerators. 

// When not to use pass by value:
//     When passing arrays, structs, or classes.


// Advantages of passing by reference:
//     It allows a function to change the value of the argument, which is sometimes useful. Otherwise, const references can be used to guarantee the function won’t change the argument.
//     Because a copy of the argument is not made, it is fast, even when used with large structs or classes.
//     References can be used to return multiple values from a function.
//     References must be initialized, so there’s no worry about null values. 

// Disadvantages of passing by reference:
//     Because a non-const reference cannot be made to an rvalue (e.g. a literal or an expression), reference arguments must be normal variables.
//     It can be hard to tell whether a parameter passed by non-const reference is meant to be input, output, or both.
//     Judicious use of const and a naming suffix for out variables can help.
//     It’s impossible to tell from the function call whether the argument may change. An argument passed by value and passed by reference looks the same.
//     We can only tell whether an argument is passed by value or reference by looking at the function declaration.
//     This can lead to situations where the programmer does not realize a function will change the value of the argument. 

// When to use pass by reference:
//     When passing structs or classes (use const if read-only).
//     When you need the function to modify an argument.

// When not to use pass by reference:
//     When passing fundamental types (use pass by value).


// Advantages of passing by address:
//     It allows a function to change the value of the argument, which is sometimes useful. Otherwise, const can be used to guarantee the function won’t change the argument.
//     Because a copy of the argument is not made, it is fast, even when used with large structs or classes.
//     We can return multiple values from a function. 

// Disadvantages of passing by address:
//     Because literals and expressions do not have addresses, pointer arguments must be normal variables.
//     All values must be checked to see whether they are null. Trying to dereference a null value will result in a crash. It is easy to forget to do this.
//     Because dereferencing a pointer is slower than accessing a value directly, accessing arguments passed by address is slower than accessing arguments passed by value. 

// When to use pass by address:
//     When passing pointer values.
//     When passing built-in arrays (if you’re okay with the fact that they’ll decay into a pointer).

// When not to use pass by address:
//     When passing structs or classes (use pass by reference).
//     When passing fundamental types (use pass by value).

// As you can see, pass by address and pass by reference have almost identical advantages and disadvantages.
// Because pass by reference is generally safer than pass by address, pass by reference should be preferred in most cases.
// Rule: Prefer pass by reference to pass by address whenever applicable.



// When to use return by value:
//     When returning variables that were declared inside the function
//     When returning function arguments that were passed by value

// When not to use return by value:
//     When returning a built-in array or pointer (use return by address)
//     When returning a large struct or class (use return by reference)


// When to use return by address:
//     When returning dynamically allocated memory
//     When returning function arguments that were passed by address

// When not to use return by address:
//     When returning variables that were declared inside the function (use return by value)
//     When returning a large struct or class (use return by reference)


// When to use return by reference:
//     When returning a reference parameter
//     When returning a large struct or class that has scope outside of the function

// When not to use return by reference:
//     When returning variables that were declared inside the function (use return by value)
//     When returning a built-in array or pointer value (use return by address)



// The use of functions provides many benefits, including:
//     The code inside the function can be reused.
//     It is much easier to change or update the code in a function (which needs to be done once) than for every in-place instance. Duplicate code is a recipe for disaster.
//     It makes your code easier to read and understand, as you do not have to know how a function is implemented to understand what it does.
//     The function provides type checking. 

// However, one major downside of functions is that every time a function is called, there is a certain amount of performance overhead that occurs.
// This is because the CPU must store the address of the current instruction it is executing (so it knows where to return to later) along with other registers,
// all the function parameters must be created and assigned values, and the program has to branch to a new location. Code written in-place is significantly faster.

// For functions that are large and/or perform complex tasks, the overhead of the function call is usually insignificant compared to the amount of time the function takes to run. 
// owever, for small, commonly-used functions, the time needed to make the function call is often a lot more than the time needed to actually execute the function’s code. 
// his can result in a substantial performance penalty.

// C++ offers a way to combine the advantages of functions with the speed of code written in-place: inline functions.
// The inline keyword is used to request that the compiler treat your function as an inline function. When the compiler compiles your code,
// all inline functions are expanded in-place -- that is, the function call is replaced with a copy of the contents of the function itself, which removes the function call overhead!
// The downside is that because the inline function is expanded in-place for every function call, this can make your compiled code quite a bit larger,
// especially if the inline function is long and/or there are many calls to the inline function.



// Function overloading is a feature of C++ that allows us to create multiple functions with the same name, so long as they have different parameters.
// Function overloading can lower a program’s complexity significantly while introducing very little additional risk. Function overloading typically works transparently
// and without any issues. The compiler will flag all ambiguous cases, and they can generally be easily resolved.
// Rule: use function overloading to make your program simpler.


// A default parameter (also called an optional parameter or a default argument) is a function parameter that has a default value provided to it.
// If the user does not supply a value for this parameter, the default value will be used.
// If the user does supply a value for the default parameter, the user-supplied value is used instead of the default value.

// Once declared, a default parameter can not be redeclared. That means for a function with a forward declaration and a function definition,
// the default parameter can be declared in either the forward declaration or the function definition, but not both.
// Best practice is to declare the default parameter in the forward declaration and not in the function definition,
// as the forward declaration is more likely to be seen by other files (particularly if it’s in a header file).

// Note that it is impossible to supply a user-defined value for z without also supplying a value for x and y.
// This is because C++ does not support a function call syntax such as printValues(,,3). This has two major consequences:
// All default parameters must be the rightmost parameters.
// If more than one default parameter exists, the leftmost default parameter should be the one most likely to be explicitly set by the user.
// Rule: If the function has a forward declaration, put the default parameters there. Otherwise, put them in the function definition.

#include <iostream>
#include <algorithm>
#include <utility>
#include <functional>


// tempPtr is now a reference to a pointer, so any changes made to tempPtr will change the argument as well!
void setToNull(int *&tempPtr)
{
    tempPtr = 0; // use 0 instead if not C++11
}


int returnByValue()
{
    return 5;
}
 
int& returnByReference()
{
     static int x = 5; // static ensures x isn't destroyed when it goes out of scope
     return x;
}


// Here is a comparison function that sorts in ascending order
// (Note: it's exactly the same as the previous ascending() function)
bool ascending(int x, int y)
{
    return x > y; // swap if the first element is greater than the second
}
 
// Here is a comparison function that sorts in descending order
bool descending(int x, int y)
{
    return x < y; // swap if the second element is greater than the first
}

// Note our user-defined comparison is the third parameter
void selectionSort(int *array, int size,  bool (*comparisonFcn)(int, int) = ascending)
{
    // Step through each element of the array
    for (int startIndex = 0; startIndex < size; ++startIndex)
    {
        // bestIndex is the index of the smallest/largest element we've encountered so far.
        int bestIndex = startIndex;
 
        // Look for smallest/largest element remaining in the array (starting at startIndex+1)
        for (int currentIndex = startIndex + 1; currentIndex < size; ++currentIndex)
        {
            // If the current element is smaller/larger than our previously found smallest
            if (comparisonFcn(array[bestIndex], array[currentIndex])) // COMPARISON DONE HERE
                // This is the new smallest/largest number for this iteration
                bestIndex = currentIndex;
        }
 
        // Swap our start element with our smallest/largest element
        std::swap(array[startIndex], array[bestIndex]);
    }
}
 
// This function prints out the values in the array
void printArray(int *array, int size)
{
    for (int index=0; index < size; ++index)
        std::cout << array[index] << " ";
    std::cout << '\n';
}

int foo()
{
    return 5;
}
 
int goo()
{
    return 6;
}


int main(void)
{
	// First we set ptr to the address of five, which means *ptr = 5
    int five = 5;
    int *ptr = &five;
	
    // This will print 5
    std::cout << *ptr << "\n";
 
    // tempPtr is set as a reference to ptr
    setToNull(ptr);
 
    // ptr has now been changed to nullptr!
 
    if (ptr)
        std::cout << *ptr << "\n";
    else
        std::cout << "ptr is null\n";


    int value = returnByReference(); // case A -- ok, treated as return by value
    // int &ref = returnByValue(); // case B -- compile error
    const int &cref = returnByValue(); // case C -- ok, the lifetime of return value is extended to the lifetime of cref


    int array[9] = { 3, 7, 9, 5, 6, 1, 8, 2, 4 };
 
    // Sort the array in descending order using the descending() function
    selectionSort(array, 9, descending);
    printArray(array, 9);
 
    // Sort the array in ascending order using the ascending() function
    selectionSort(array, 9, ascending);
    printArray(array, 9);

    std::function<int()> fcnPtr; // declare function pointer that returns an int and takes no parameters
    fcnPtr = goo; // fcnPtr now points to function goo
    std::cout << fcnPtr(); // call the function just like normal


    return 0;
}