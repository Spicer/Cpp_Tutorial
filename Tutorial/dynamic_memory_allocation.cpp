// Most normal variables (including fixed arrays) are allocated in a portion of memory called the stack. The amount of stack memory for a program is generally quite small -- 
// Visual Studio defaults the stack size to 1MB. If you exceed this number, stack overflow will result, and the operating system will probably close down the program.

// Dynamic memory allocation is a way for running programs to request memory from the operating system when needed.
// This memory does not come from the program’s limited stack memory -- instead, it is allocated from a much larger pool of memory managed by the operating system called the heap.
// On modern machines, the heap can be gigabytes in size.

// The new operator returns a pointer containing the address of the memory that has been allocated.

// A pointer that is pointing to deallocated memory is called a dangling pointer. Dereferencing or deleting a dangling pointer will lead to undefined behavior.
// Rule: To avoid dangling pointers, after deleting memory, set all pointers pointing to the deleted memory to 0 (or nullptr in C++11).

// By default, if new fails, a bad_alloc exception is thrown. If this exception isn’t properly handled the program will simply terminate (crash) with an unhandled exception error.
// In many cases, having new throw an exception (or having your program crash) is undesirable, so there’s an alternate form of new that can be used instead
// to tell new to return a null pointer if memory can’t be allocated. This is done by adding the constant std::nothrow between the new keyword and the allocation type:


// A dynamic array starts its life as a pointer that points to the first element of the array. Consequently, it has the same limitations in that it doesn’t know its length or size.
// A dynamic array functions identically to a decayed fixed array, with the exception that the programmer is responsible for deallocating the dynamic array via the delete[] keyword.

#include <iostream>
#include <string>

// Selection sort algorithm
void sortArray(std::string *array, int length)
{
	// Step through each element of the array
	for (int startIndex = 0; startIndex < length; ++startIndex)
	{
		// smallestIndex is the index of the smallest element we've encountered so far.
		int smallestIndex = startIndex;
 
		// Look for smallest element remaining in the array (starting at startIndex+1)
		for (int currentIndex = startIndex + 1; currentIndex < length; ++currentIndex)
		{
			// If the current element is smaller than our previously found smallest
			if (array[currentIndex] < array[smallestIndex])
				// This is the new smallest number for this iteration
				smallestIndex = currentIndex;
		}
 
		// Swap our start element with our smallest element
		std::swap(array[startIndex], array[smallestIndex]);
	}
}

int main(void)
{
	int *ptr1 = new int (5); // use direct initialization
	// int *ptr2 = new int { 6 }; // use uniform initialization	

	// assume ptr has previously been allocated with operator new
	delete ptr1; // return the memory pointed to by ptr to the operating system
	ptr1 = 0; // set ptr to be a null pointer (use nullptr instead of 0 in C++11)

	int *value = new (std::nothrow) int; // value will be set to a null pointer if the integer allocation fails
	if (!value) // handle case where new returned null
	{
	    std::cout << "Could not allocate memory";
	    // exit(1);
	}
	delete value;
	value = 0;


	std::cout << "Enter a positive integer: ";
    int length;
    std::cin >> length;
 
    int *array = new int[length]; // use array new.  Note that length does not need to be constant!
 
    std::cout << "I just allocated an array of integers of length " << length << '\n';
 
    array[0] = 5; // set element 0 to value 5
 
    delete[] array; // use array delete to deallocate array
    array = 0; // use nullptr instead of 0 in C++11


    std::cout << "How many names would you like to enter? ";
	std::cin >> length;
 
	// Allocate an array to hold the names
	std::string *names = new std::string[length];
 
	// Ask user to enter all the names
	for (int i = 0; i < length; ++i)
	{
		std::cout << "Enter name #" << i + 1 << ": ";
		std::cin >> names[i];
	}
 
	// Sort the array
	sortArray(names, length);
 
	std::cout << "\nHere is your sorted list:\n";
	// Print the sorted array
	for (int i = 0; i < length; ++i)
		std::cout << "Name #" << i + 1 << ": " << names[i] << '\n';
 
	delete[] names; // don't forget to use array delete
        names = 0; // use 0 if not C++11

	return 0;
}