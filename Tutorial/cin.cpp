#include <iostream>
 
double getDouble()
{
    while (true) // Loop until user enters a valid input
    {
        std::cout << "Enter a double value: ";
        double x;
        std::cin >> x;
 
        // Check for failed extraction
        if (std::cin.fail()) // has a previous extraction failed?
        {
            // yep, so let's handle the failure
            std::cin.clear(); // put us back in 'normal' operation mode
            std::cin.ignore(32767,'\n'); // and remove the bad input
            std::cout << "Oops, that input is invalid.  Please try again.\n";
        }
        else
        {
            std::cin.ignore(32767,'\n'); // remove any extraneous input
 
            // the user can't enter a meaningless double value, so we don't need to worry about validating that
            return x;
        }
    }
}
 
char getOperator()
{
    while (true) // Loop until user enters a valid input
    {
        std::cout << "Enter one of the following: +, -, *, or /: ";
        char op;
        std::cin >> op;
 
        // Chars can accept any single input character, so no need to check for an invalid extraction here
 
        std::cin.ignore(32767,'\n'); // remove any extraneous input
 
        // Check whether the user entered meaningful input
        if (op == '+' || op == '-' || op == '*' || op == '/')    
            return op; // return it to the caller
        else // otherwise tell the user what went wrong
            std::cout << "Oops, that input is invalid.  Please try again.\n";
        } // and try again
}
 
void printResult(double x, char op, double y)
{
    if (op == '+')
        std::cout << x << " + " << y << " is " << x + y << '\n';
    else if (op == '-')
        std::cout << x << " - " << y << " is " << x - y << '\n';
    else if (op == '*')
        std::cout << x << " * " << y << " is " << x * y << '\n';
    else if (op == '/')
        std::cout << x << " / " << y << " is " << x / y << '\n';
    else // Being robust means handling unexpected parameters as well, even though getOperator() guarantees op is valid in this particular program
        std::cout << "Something went wrong: printResult() got an invalid operator.";
 
}
 
int main()
{
    double x = getDouble();
    char op = getOperator();
    double y = getDouble();
 
    printResult(x, op, y);


    // Bad way:
    std::cout << "Enter your full name: ";
    std::string name;
    std::cin >> name; // this won't work as expected since std::cin breaks on whitespace
 
    std::cout << "Enter your age: ";
    std::string age;
    std::cin >> age;
 
    std::cout << "Your name is " << name << " and your age is " << age;

    // Better way:
    std::cout << "Enter your full name: ";
    std::getline(std::cin, name); // read a full line of text into name
 
    std::cout << "Enter your age: ";
    std::getline(std::cin, age); // read a full line of text into age
 
    std::cout << "Your name is " << name << " and your age is " << age;

    // Getting integers with cin doesn't work as expected unless we ignore the \n character at the end of the number
    std::cout << "Pick 1 or 2: ";
    int choice;
    std::cin >> choice;
 
    std::cin.ignore(32767, '\n'); // ignore up to 32767 characters until a \n is removed
 
    std::cout << "Now enter your name: ";
    std::getline(std::cin, name);
 
    std::cout << "Hello, " << name << ", you picked " << choice << '\n';
    
 
    return 0;
}